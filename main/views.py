from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor, as_completed
from .models import YtCategory, YtSearchUser, YtVideo, YtWords
from django.contrib.auth.models import User
from .forms import YtSearchUserForm
from datetime import date
from dateutil.relativedelta import relativedelta
import requests
import time

# Create your views here.
def home(request):
    user_guest = User.objects.filter(username='Guest')
    count_page, max_video = 1, 200
    information = 'Information here ...'

    if not user_guest:
        context = {'error': 'There is not user, create with the name \'Guest\'.', 'information': information}
        return render(request, 'main/detail.html', context)
    else:
        user_guest = user_guest[0]
        search_user = list(YtSearchUser.objects.filter(user_id=user_guest))

    form = YtSearchUserForm()

    if request.method == "POST":
        form = YtSearchUserForm(request.POST)
        if form.is_valid():
            new = form.save(commit=False)
            # if already exist the user in this form
            search_user_list_id = list(YtSearchUser.objects.filter(user_id=user_guest))
            if search_user_list_id:
                search_user = search_user_list_id[0]
                search_user.name = new.name
                search_user.mond_min = new.mond_min
                search_user.tues_min = new.tues_min
                search_user.wedn_min = new.wedn_min
                search_user.thur_min = new.thur_min
                search_user.frid_min = new.frid_min
                search_user.satu_min = new.satu_min
                search_user.sund_min = new.sund_min
                search_user.user_id = user_guest
                search_user.save()
                new = search_user
            else:
                new.information = information
                new.user_id = user_guest
                new.save()

            time_ini = time.time()
            # list_dict_video = search_videos_words_youtube(new, count_page, max_video)
            list_dict_video = search_videos_words_youtube_with_thread(new, max_video)
            time_end = time.time()
            print ("time: " + str(time_end - time_ini))

            create_videos_from_youtube(list_dict_video, user_guest)
            create_category_and_words(list_dict_video, user_guest)
            return redirect('search/detail/')
    else:
        form = YtSearchUserForm()
    
    information = search_user[0].information if search_user else information
    return render(request, 'main/index.html', {'form': form, 'information': information})

def new_search_detail(request):
    user_guest = User.objects.filter(username='Guest')
    if not user_guest:
        context = {'error': 'There is not user, create with the name \'Guest\'.'}
        return render(request, 'main/detail.html', context)
    else:
        user_guest = user_guest[0]

    context, ctx_video = {}, {}
    search_user = YtSearchUser.objects.filter(user_id=user_guest)[0]
    category = YtCategory.objects.filter(yt_search_id=search_user.id)
    for catg in category:
        context[catg.name] = YtWords.objects.filter(category_id=catg)

    context['name_search'] = search_user.name
    context['all_video'] = YtVideo.objects.all().order_by('date_month', 'id')
    context['information'] = search_user.information
    all_dates = sorted(set(list(map(lambda d: d.date_month, YtVideo.objects.all()))))
    context['qt_days'] = '0' if not all_dates else '1' if len(all_dates) == 1 else str((all_dates[-1] - all_dates[0]).days)

    return render(request, 'main/new_search_detail.html', context)

def get_info(url, timeout):
    print (url)
    return requests.get(url, timeout = timeout)

def requestUrls(urls, timeout):
    with ThreadPoolExecutor(max_workers=5) as executor:
        agenda = { executor.submit(get_info, url, timeout): url for url in urls }        
        dict_compl = as_completed(agenda)
        for tarefa in sorted(agenda, key=agenda.get):     
            try:
                conteudo = tarefa.result()
            except Exception as e:
                print ("Não foi possível fazer a requisição! \n{}".format(e))
            else:
                yield conteudo

def search_videos_words_youtube_with_thread(search_user, max_video):
    page_link = 'https://www.youtube.com/results?search_query=' + '+' + search_user.name.replace(' ', '+') + '&page='
    urls = map(lambda count: page_link + str(count + 1), range(30))
    timeout = 10
    requisicoes = requestUrls(urls, timeout) # timeout é opcional, o padrão é 5
    list_dict_video = []

    for requisicao in requisicoes:
        soup = BeautifulSoup(requisicao.content, 'html.parser').body.findAll('div',attrs={'class':'yt-lockup-content'})
        if len(soup) == 0:
            break
        
        list_dict_video = mount_list_dict_video(max_video, list_dict_video, soup)
        if len(list_dict_video) >= max_video:
            break
    return list_dict_video[0:max_video]

def search_videos_words_youtube(search_user, count_page, max_video):
    list_dict_video = []
    while(True):
        page_link = 'https://www.youtube.com/results?search_query=' + '+' + search_user.name.replace(' ', '+') + '&page=' + str(count_page)
        page = requests.get(page_link)
        print ('status code: ' + str(page.status_code))
        soup = BeautifulSoup(page.content, 'html.parser').body.findAll('div',attrs={'class':'yt-lockup-content'})
        if len(soup) == 0:
            break
        
        list_dict_video = mount_list_dict_video(max_video, list_dict_video, soup)
        if len(list_dict_video) >= max_video:
            break
        count_page += 1
    return list_dict_video

def create_category_and_words(list_dict_video, user_guest):
    """ create words more repeated to each category """

    YtCategory.objects.all().delete()

    search_user = YtSearchUser.objects.filter(user_id=user_guest)[0]
    category = YtCategory.objects.filter(yt_search_id=search_user.id)
    dict_words = create_dict_words_more_repeated(list_dict_video)
    if not category:
        for catg in dict_words:
            category_id = YtCategory(name=catg, yt_search_id=search_user)
            category_id.save()
            for word in dict_words[catg]:
                word_id = YtWords(name=word, category_id=category_id)
                word_id.save()

def create_videos_from_youtube(list_dict_video, user_guest):
    """ create_videos_from_youtube """

    YtVideo.objects.all().delete()

    search_user = YtSearchUser.objects.filter(user_id=user_guest)[0]
    list_time_wkd = [search_user.mond_min, search_user.tues_min, search_user.wedn_min, search_user.thur_min, search_user.frid_min, search_user.satu_min, search_user.sund_min]
    list_time_wkd = list(map(lambda x: x*60, list_time_wkd))
    max_time = max(list_time_wkd)
    list_time_all_video = list(filter(lambda n: n <= max_time, map(lambda d: d['time']['value'], list_dict_video)))
    stop_index, last_index = 0, len(list_time_all_video)-1
    today, list_dict_info, is_finished = date.today(), [], False
    page_link = 'https://www.youtube.com'
    context = {}

    while True:
        for day_index in range(len(list_time_wkd)):
            time_today = list_time_wkd[day_index]
            list_time_all_video_aux = list_time_all_video[stop_index:]
            list_time = []
            
            for index in range(len(list_time_all_video_aux)):
                time = list_time_all_video_aux[index]
                if time <= time_today:
                    time_today -= time
                    list_time.append(index + stop_index)
                    if stop_index + index == last_index:
                        stop_index += index
                else:
                    stop_index += index
                    break

            for list_time_index in list_time:
                video_name = list_dict_video[list_time_index]['title']
                video_link = list_dict_video[list_time_index]['link']
                time_sec = list_dict_video[list_time_index]['time']['value']
                time_str = list_dict_video[list_time_index]['time']['time']
                video_id = YtVideo(name=video_name, linkpage=page_link+video_link, date_month=today, time_sec=time_sec, time_str=time_str, yt_search_id=search_user)
                video_id.save()

            if stop_index == last_index and list_time:
                break
            today += relativedelta(days=+1)
        if stop_index == last_index and list_time:
            break

def mount_list_dict_video(max_video, list_dict_video, soup):
    for video in soup:
        link = str(video.a.get('href'))
        title = str(video.a.string)
        description = video.find('div', class_='yt-lockup-description')
        description = str(description.get_text()) if description != None and description.get_text() != None else ''
        time_video = video.find('span', class_='accessible-description')
        time_video = time_video.string if time_video != None and time_video.string != None else ''

        # warning: '' = long video
        if link.__contains__('www.googleadservices.com') or time_video.__contains__('Playlist') or time_video == '':
            continue
        
        if time_video.__contains__('Canal'):
            continue

        time_video = time_video.replace(' ', '').replace('-', '').replace('Duração:', '').replace('.', '')
        dict_time_video = {'time': time_video}
        time_video = time_video.split(':')
        dict_time_video['value'] = int(time_video[-1]) + int(time_video[-2])*60 + (int(time_video[-3])*3600 if len(time_video) > 2 else 0)

        list_dict_video.append({'title': title, 'link': link, 'description': description, 'time': dict_time_video})
        if len(list_dict_video) >= max_video:
            return list_dict_video
    
    return list_dict_video

def create_dict_words_more_repeated(list_dict_video):
    dict_words = {'title': {}, 'description': {}, 'general': {}}

    # create lists of all words from titles and descriptions
    list_title_words = ' '.join(list(map(lambda d: d['title'].replace('-', ' ').replace('|', ' ').replace('\'', ' ').replace('\xa0...', ' ').replace(',', ' ').replace('(', ' ').replace(')', ' ').lower().strip(), list_dict_video))).split(' ')
    list_descrip_words = ' '.join(list(map(lambda d: d['description'].replace('-', ' ').replace('|', ' ').replace('\'', ' ').replace('\xa0...', ' ').replace(',', ' ').replace('(', ' ').replace(')', ' ').lower().strip(), list_dict_video))).split(' ')
    
    for kind_word in ['title', 'description', 'general']:
        list_word = list_title_words if kind_word == 'title' else list_descrip_words if kind_word == 'description' else (list_title_words + list_descrip_words)
        # create dictionaries to contain the amount of all words in titles, descriptions and in general
        list(map(lambda key: dict_words[kind_word].update({key: dict_words[kind_word].get(key, 0) + 1}) if key not in ['', ' '] else None, list_word))
        # create lists with the five most repeated words for title, description and general
        dict_words[kind_word] = list(sorted(dict_words[kind_word], key=dict_words[kind_word].get)[::-1][0:5])

    return dict_words

def show_page_not_found(request, info):
    search_user = YtSearchUser.objects.all()
    information = search_user[0].information if search_user else 'Information here ...'
    error = info[0] if str(type(info)).__contains__("list") and info else "Page not found. Looks like you followed a bad link."
    return render(request, 'main/detail.html', {'error': error, 'information': information})