from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.core.validators import URLValidator


# Create your models here.
class YtSearchUser(models.Model):
    name = models.CharField(max_length=30)
    sund_min = models.PositiveIntegerField(default=0)
    mond_min = models.PositiveIntegerField(default=0)
    tues_min = models.PositiveIntegerField(default=0)
    wedn_min = models.PositiveIntegerField(default=0)
    thur_min = models.PositiveIntegerField(default=0)
    frid_min = models.PositiveIntegerField(default=0)
    satu_min = models.PositiveIntegerField(default=0)
    information = models.TextField(default=None)
    user_id = models.OneToOneField(User, on_delete=models.CASCADE, default=None)

    def publish(self):
        self.save()

    def __str__(self):
        return self.name

class YtVideo(models.Model):
    name = models.CharField(max_length=300)
    linkpage = models.CharField(max_length=200, default=None)
    date_month = models.DateField(default=None)
    time_sec = models.PositiveIntegerField(default=None)
    time_str = models.CharField(max_length=20, default=None)
    yt_search_id = models.ForeignKey('YtSearchUser', on_delete=models.CASCADE, related_name='ytvideo')

    def publish(self):
        self.save()

    def __str__(self):
        return self.name

class YtCategory(models.Model):
    name = models.CharField(max_length=30)
    yt_search_id = models.ForeignKey('YtSearchUser', on_delete=models.CASCADE, related_name='ytcategory')

    def publish(self):
        self.save()

    def __str__(self):
        return self.name

class YtWords(models.Model):
    name = models.CharField(max_length=30)
    category_id = models.ForeignKey('YtCategory', on_delete=models.CASCADE, related_name='ytwords')

    def publish(self):
        self.save()

    def __str__(self):
        return self.name