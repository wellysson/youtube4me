from django.contrib import admin
from .models import YtCategory, YtSearchUser, YtVideo, YtWords

# Register your models here.
admin.site.register(YtCategory)
admin.site.register(YtSearchUser)
admin.site.register(YtVideo)
admin.site.register(YtWords)