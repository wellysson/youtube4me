from django.forms import ModelForm
from .models import YtSearchUser

class YtSearchUserForm(ModelForm):
    class Meta:
        model = YtSearchUser
        fields = ('name', 'sund_min', 'mond_min', 'tues_min', 'wedn_min', 'thur_min', 'frid_min', 'satu_min')
