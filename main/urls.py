from django.contrib import admin
from django.urls import path, re_path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('search/detail/', views.new_search_detail, name='new_search_detail'),

    # pages not found
    re_path(r'()/', views.show_page_not_found, name='show_page_not_found'),
]
