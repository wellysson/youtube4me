>>>>>>>>>> Comments <<<<<<<<<<<

- This site made in django aims to access the site https://www.youtube.com/ and
get the videos referring to the user profile.

- The site was designed to work only with one user, called 'Guest', if it does
not exist the site will redirect to an error page. However the project will be
demonstrated with this user already created.

- It will only be possible to store a search, the site was done only with the
purpose of demonstrating the result so it was not created a login page for each
user to have their search the first moment.

- The site will only display information when performing the search, the search
will return a maximum of 200 videos to be analyzed correctly.

- Responsive website, thus adapting to different screen sizes.

>>>>>>>> Future Improvements <<<<<<<<<<

- Create a login page to register user where each user will have a profile to
register more than one search.